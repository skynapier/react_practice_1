import React, { Component } from 'react';

import './asserts/css/App.css';
import TestComponents1 from "./components/Test_Components_1";
import TestComponents2 from "./components/Test_Components_2";
import TestEvent1 from "./components/Test_Event_1";
import TestEvent2 from './components/Test_Event_2'
import TestForm1 from './components/Test_Form_1';


class App extends Component {
  render() {
    return (
      <div className="App">
      This is the root
      <hr />
      <TestComponents1 />
      <hr/>
      <TestEvent1 />
      <hr />
      <TestEvent2 />
      <hr />
      <TestComponents2 />
      <hr ></hr>
      <TestForm1></TestForm1>
      </div>
    );
  }
}

export default App;
