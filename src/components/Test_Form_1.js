import React, {Component} from 'react';


class Test_Form_1 extends Component {
    constructor(props) {
        super(props);
        this.state = { 

            msg:"React Form",
            name:'',
            sex: '1',
            city:'',
            cities:[
                'Auckland','Wellington','ChristChurch'
            ],
            hobby:[
                {'title':'sleep','checked':true},
                {'title':'dine','checked':false},
                {'title':'coding','checked':true}

            ],
            info:''
         };
         this.handleInfo = this.handleInfo.bind(this);
    }

    handleSubmit=(e)=>{
        e.preventDefault();
        console.log(this.state.name,this.state.sex,this.state.city,this.state.hobby,this.state.info);
    }

    handleName=(e)=>{
        this.setState({
           name:e.target.value 
        });
    }

    handleSex=(e)=>{
        this.setState({
            sex:e.target.value
        });
    }
    
    handleCity=(e)=>{
        this.setState({
            city:e.target.value
        });
    }

    handleHobby(key){
        var hobby = this.state.hobby;
        // set it as it opposite
        hobby[key].checked =! hobby[key].checked;
        this.setState(
            {
                hobby:hobby
            }
        )
    }

    handleInfo(e){
        this.setState({
            info:e.target.value
        })
    }


    render() {
        return (
            <div>
                <h2>{this.state.msg}</h2>

                <form onSubmit={this.handleSubmit}>
                    Username: <input type="text" value={this.state.name} onChange={this.handleName}></input>
                    <br></br>
                    <br></br>

                    Gender: <input type="radio" value='1' checked={this.state.sex==1} onChange={this.handleSex}></input> male
                            <input type="radio" value='2' checked={this.state.sex==2} onChange={this.handleSex}></input> female
                    <br></br>
                    <br></br>

                    Living City:
                            <select value={this.state.city} onChange={this.handleCity}>
                            {
                                this.state.cities.map(function(value,key){
                                    return <option key={key}>{value}</option>
                                })
                            }
                            </select>
                    <br></br>
                    <br></br>

                    Hobby: 
                        {   
                            //caution this point
                            this.state.hobby.map((value,key)=>{
                                return(
                                    <span key={key}>
                                     
                                     <input type="checkbox" checked={value.checked} onChange={this.handleHobby.bind(this,key)} ></input>
                                     {value.title}
                                    </span>
                                )
                            })
                        }
                <br></br>
                <br></br>

                info:
                        <textarea value={this.state.info} onChange={this.handleInfo}></textarea>


                <input type="submit" defaultValue="Submit"></input>
                </form>
            </div>
        );
    }
}

export default Test_Form_1;