import React, { Component} from 'react';

/**
 * Constraint and un-Constraint component
 * 
 *      un-Constarint component: <input type="text" defaultValue="a" />
 *      This defaultValue actrually is the origional value in the DOM.
 *      This value is the value that people input,
 *      React will not manage its input procedure in this component.
 * 
 *      Constraint component: <input value={this.state.username} type="text" onChange={this.handleUsername} />
 *      This value is not a constant value, it is this.state.username.
 *      this.state.username is manage by this.handleChange.
 *      This input value is not people input one, because when the onChange event happend,
 *      this.setSate will cause a new render. React will optimise its render procedure.
 *      looks like binary data binding, 
 *      
 */

class Test_Components_2 extends Component {
    constructor(props) {
        super(props);
        this.state = {  
            username: 'test components 2'
        };
    }

    handleUsername=(e)=>{
        this.setState(
            {
                username:e.target.value
            }
        )
    }

    render() {
        return (
            <div>

                <h1>Test Constraint and un-Constraint Component</h1>
                
                {/* MVVM */}

                <input type="text" value={this.state.username} onChange={this.handleUsername} />

                <p > Test MVVM username: {this.state.username} </p>
                
                <br></br>

                {/* MV */}

                <input type="text" defaultValue={this.state.username}></input>

                <p > Test MV username: {this.state.username} </p>


            </div>
        );
    }




}

export default Test_Components_2;