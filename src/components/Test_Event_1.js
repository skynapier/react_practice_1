import React from 'react';
/**
 * This Component learn how to build event
 * 3 approaches for invoke data from statement
 * 1 approach for change data in the statement
 */

class Test_Event_1 extends React.Component {

    constructor(props){
        super(props);
        this.state={
            msg:"I'm a event component",
            data1:"proof get right 1",
            data2:"proof get right 2",
            data3:"proof get right 3"
        }
        this.get_statement_data2=this.get_statement_data2.bind(this);
    }
    

    
    run(){
        alert("I'm a event ")
    }
    
    get_statement_data1(){
        alert(this.state.data1);
    }

    get_statement_data2(){
        alert(this.state.data2);
    }

    get_statement_data3=()=>{
        alert(this.state.data3);
    }

    change_statement_msg=()=>{
        this.setState({
            msg:"proof you have change msg in statement"
            
        })
       
    }
    
    set_statement_msg=(str1,str2)=>{
        this.setState({
            msg:str1 +'\n' + str2
        })
    }

    restore_statement_msg=()=>{
        this.setState({
            msg:"I'm a event component"
           
        })
    }
    render(){
        return(
            <div>
            <h2>{this.state.msg}</h2>
            <button onClick={this.run}> Execution </button>
            <br />
            <button onClick={this.get_statement_data1.bind(this)}> Test Get Data from statement first approach </button>
            <br />
            <button onClick={this.get_statement_data2}> Test Get Data from statement second approach </button>
            <br />
            <button onClick={this.get_statement_data3}> Test Get Data from statement third approach </button>
            <br />
            <button onClick={this.change_statement_msg}> Test change Data in the statement</button>
            <br />
            <button onClick={this.restore_statement_msg}> Test restore Data in the statement</button>
            <br />
            <button onClick={this.set_statement_msg.bind(this,"this is proof that you set a new value","second value")}> Test set Data in the statement</button>
            
            </div>

        )
        
    }


}

export default Test_Event_1;