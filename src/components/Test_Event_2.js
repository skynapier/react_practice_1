import React from 'react';

/**
 * Event object, Key boad Event, Form Event, Ref get Dom node
 * Give one label properties such as id
 * React implement Model and View coordinate
 * 
 */


 class Test_Event_2 extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            msg:'this is message',
            username:'original'

        }

    }
    
    run=(event)=>{
        //alert(event.target);
        //console.log(event);
        event.target.style.background = 'red';

        //get DOM property
        alert(event.target.getAttribute('aid'))
    }

    inputChange= (e)=>{
        
        console.log(e.target.value);

        this.setState(
            { username:e.target.value}
        )
    }
    
    getInput= () =>{
        alert("test by get state", +
        this.state.username);
    }

    inputChangeRef= () =>{

        let val = this.refs.username.value;
        this.setState({
            username:val
        })
    }

    inputkeyUp=(e)=>{
       
        /* 13 is enter*/

        if(e.keyCode === 13){
            alert(e.target.value ); 
        }
    }

    render(){
        return(
            <dir>
                {this.state.msg}
                <br />
                {/* this is comment */}
                <br />
                <h2> Show the object</h2>

                {/*give button one property 'aid'*/}
                <button aid='123' onClick={this.run}> event object1 </button>
                <br />

                <h2> Form Event</h2>
                <h2 >2 approaches</h2>
                <li >1 get current dom node </li>
                <li >replace value in the state </li> 

                <h2> For Get Form event value</h2>
                <li> monitor form change on event object</li>
                <li> Get value from the changed event object</li>
                <li> Change the pre-set value in the state by the getted value above</li>
                <li> get value in state when click button </li>
                <input onChange ={this.inputChange}/> <button onClick={this.getInput}> Get Value when click button </button>
                <br></br>

                <h2> Get value from ref</h2>
                <li> Give element a ref property input ref= username </li>
                <li> this.refs.username to get dom node</li>
                <input ref='username' onChange ={this.inputChange}/> <button onClick={this.getInput}> Get Value when click button via ref</button>
                
                <br></br>

                <h2> Event occur when key up</h2>
                <li> KeyUp and KeyDown are same with Java</li>
                <li> event.keyCode is ASCII 2 code</li>
                <input onKeyUp={this.inputkeyUp} />

                <br></br>
                <h2> binary data change MVVM</h2>
                <p> Model change influence view then view replace value in Model</p>
                <input value={this.state.username} onChange={this.inputChange}/>
                <p> current state: {this.state.username} </p>


            
            </dir>

        );

    }


 }
 export default Test_Event_2;