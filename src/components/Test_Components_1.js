import React from 'react';
import logo from '../logo.svg';
import '../asserts/css/App.css';
import '../asserts/css/testcolor.css';

class Test_Components_1 extends React.Component {
    constructor(props){
        super(props);
        this.state={
            title: "Breakfast Kitchen",
            name: "amy",
            age: "23",
            color: 'red',
            image1: "http://mmbiz.qpic.cn/mmemoticon/C9KUPZmcW5h7bKMSe1tgc9g2ZMialW13CpVWsmsg2DBm8ECEAGZibHC8Uv3vAXd3aC/0"   
            
        };
        this.style={
            color:'yellow',
            fontSize:'50px'
        };
        this.list1=[
            {a:'0000'},
            {a:'1111'},
            {a:'2222'}
        ];
        this.list2=[
            <h2>h2 label 1</h2>,
            <h2>h2 label 2</h2>
        ];
        this.list3=[
            '3333',
            '4444',
            '5555'
        ]
    }


  render() {
    let ListResult = this.list3.map( function(value,key){
        return <li> {value}</li>
    }
    )


    return (
      <div className="Test_Components_1">
        <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            
            <a
                className="App-link"
                href="https://reactjs.org"
                target="_blank"
                rel="noopener noreferrer"
            >
                Learn React
            </a>
            <h2> All the node should be include in the root node in the react component</h2>
            
            <br />
            
            <p>Give label a property and add images</p>
            {/* this title is give its property */}
            <div 
                title={this.state.title}
                className={this.state.color}
                style={this.style}
            > 
                Waang Breakfast
                </div>
            <img src ={this.state.image1} alt="image1"/>


            <br />

            <label htmlFor="name" > 
            Name
            <input id="name" />
            </label>

            <br />

        
            <h3> iterate lists in 3 methods</h3>
            <p> using ul and map() function </p>
            <ul style ={this.style}>
                {
                    this.list1.map(function(value,key){
                        return (<li key={key}> {value.a} </li> )
                    
                    })
                } 
            </ul>

            <p > the element in the list are include in "h" label and using ul</p>
            <ul>
                {this.list2}
            </ul>
                
            <p > set a let variable </p>    
            {ListResult}
         

        </header>
      </div>
    );
  }
}

export default Test_Components_1;
