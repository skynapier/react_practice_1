# React Practice Project

This project was using for parctice component in React.

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

For my personal understanding, the React Framework with JSX like a View in the MVC pattern. 
If one website has a common component at the front when people jump into other link it needs copy this part of component in HTML.
For example, every search page on the Google have a search bar at the front. React deal with this problem by build modules for each components and pages just need invoke this part of components instead of copy that part of component.

The main page in this project is APP.js, CSS codes are stored in css folder and components are stored in components folder.

Test_Components_1.js include how to add pictures in React, How to give one label a property and how to iterate list in REact.

Test_Components_2.js include what is difference in value and defaultValue in React components (i.e. constraint component and un-constraint component).

Test_Event_1.js for practice event monitor.

Test_Event_2.js for React implement binary bind data like Vue.

Test_Form_1.js for implement a From in React, get its value via submit label. 


## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).


